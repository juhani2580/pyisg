"""Main program."""

import sys
import log_utils

def print_usage(pname):
    """Print usage instructions.

    Arguments:
    pname -- program name

    """
    print("Usage: python %s <irclog_filename> <xhtml_output_filename> <channel_name> <network_name>" % pname)

def main():
    """Run the main program."""

    # Read Command-line parameters.
    if len(sys.argv) == 5:
        # IRC-log log_filename
        log_filename = sys.argv[1]
        # XHTML-output filename
        xhtml_filename = sys.argv[2]
        # Channel name
        chan_name = sys.argv[3]
        # Network name
        net_name = sys.argv[4]
    else:
        print_usage(sys.argv[0])
        exit()

    # parse the log
    log = log_utils.Log(log_filename)
    # initialize xhtml generator
    generator = log_utils.XHTMLGenerator(log, chan_name, net_name)

    #add all statistics
    generator.add_activity_by_hours()
    generator.add_most_active_nicks(20)
    generator.add_most_active_nicks_by_hours(10)
    generator.add_most_ref_nicks(10)
    generator.add_most_used_words(20)

    #write the generated xhtml page to a file
    generator.write(xhtml_filename)

if __name__ == "__main__":
    main()
