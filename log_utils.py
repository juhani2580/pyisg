"""Module for handling IRC log files."""
import re
from operator import itemgetter
from time import strftime
from cgi import escape

class Log:
    """Class for parsing irc log files."""

    def __init__(self, filename):
        """Initialize log parser.

        Arguments:
        filename -- log filename

        """
        #list of tuples for spoken lines: (time, nick, message)
        self._slines = []
        #dictionary with nicknames as keys and indices starting from zero as values
        self._nicks = dict()
        #number of nicknames
        self.num_nicks = 0

        #read all lines from the log file
        log_lines = []
        f = open(filename, "r")
        try:
            log_lines = f.readlines()
        except IOError:
            print("ERROR: Unable to read log file '%s'" % filename)
            exit()
        finally:
            f.close()

        self._parse(log_lines)

    def _parse(self, log_lines):
        """Parse log file.

        Arguments:
        filename -- lines from the log file

        """
        #TODO: implement proper clock format parsing for lines that do not
        #begin two digits that represent hours

        #pattern for parsing hour, nick and message from a log line
        pattern = re.compile(r'^(\d\d)[^<>]*<.([^<>]+)> (.*)\n?$')
        for i in log_lines:
            #get only spoken lines in a tuple: (hour, nick, message)
            sline = pattern.search(i)
            if sline and len(sline.groups()) == 3:
                #convert hour string to integer
                self._slines.append((int(sline.group(1)), sline.group(2), sline.group(3)))


        #add nicknames to the nicks dictionary
        i = 0
        for line in self._slines:
            #treat all nicks as lowercase
            nick = line[1].lower()
            if not nick in self._nicks:
                self._nicks[nick] = i
                i += 1

        # get the number of nicks for later usage
        self.num_nicks = len(self._nicks)

    def get_activity_by_hours(self):
        """Return a 24-element list with activity percentages for each hour."""
        hps = [0.0] * 24

        for line in self._slines:
            hour = int(line[0])
            if hour >= 0 and hour < 24:
                hps[hour] += 1.0

        sum_hours = sum(hps)
        return [hours / sum_hours for hours in hps]

    def get_most_active_nicks(self, limit):
        """Return a list with statistics of most active nicks.

        List consists limit amount of tuples (nick, lines, words, words/line,
        chars, chars/line, latest quote) sorted by character count.

        Arguments:
        limit -- number of rows in the list

        """
        #tuple list
        tl = [("", 0, 0, 0.0, 0, 0.0, "")] * len(self._nicks)
        #line counts
        lcs = [0] * len(self._nicks)
        #word counts
        wcs = [0] * len(self._nicks)
        #words/lines
        wsls = [0.0] * len(self._nicks)
        #char counts
        cscs = [0] * len(self._nicks)
        #chars/lines
        csls = [0.0] * len(self._nicks)
        #latest quotes
        lqs = [""] * len(self._nicks)

        #count lines, words and chars
        for line in self._slines:
            nick_index = self._nicks[line[1].lower()]
            lcs[nick_index] += 1 #add line
            for word in line[2].split():
                wcs[nick_index] += 1 #add word
                cscs[nick_index] += len(word) #add characters

        #calculate w/l and c/l ratios. get latest quotes.
        for i in self._nicks.items():
            #no lines spoken, avoid division by zero
            if lcs[i[1]] == 0:
                wsls[i[1]] = 0.0
                csls[i[1]] = 0.0
                lqs[i[1]] = ""
            else:
                #words/lines
                wsls[i[1]] = wcs[i[1]] / float(lcs[i[1]])
                #chars/lines
                csls[i[1]] = cscs[i[1]] / float(lcs[i[1]])

                #try to find the latest quote
                for line in reversed(self._slines):
                    if i[0] == line[1].lower():
                        lqs[i[1]] = line[2]
                        break

            # assign everything in a tuple (nick, lines, words, words/lines,
            # chars, chars/lines, latest quote)
            tl[i[1]] = (i[0], lcs[i[1]], wcs[i[1]], wsls[i[1]], cscs[i[1]],
                        csls[i[1]], lqs[i[1]])

        #sort by character count
        return sorted(tl, key=itemgetter(4), reverse=True)[0:limit]


    def get_most_active_nicks_by_hours(self, limit):
        """Return a list of most active nicks by hours 0-5, 6-11, 12-17 and 18-23.

        List consists limit amount of lists
        [[activity_percentage, nick, relative_activity_percentage_of_top_limit_nicks],
        [activity_percentage, nick, relative_activity_percentage_of_top_limit_nicks],
        [activity_percentage, nick, relative_activity_percentage_of_top_limit_nicks],
        [activity_percentage, nick, relative_activity_percentage_of_top_limit_nicks]]
        sorted by character count.

        Arguments:
        limit -- number of rows in the list

        """

        #TODO: make this less ugly
        #lists [[activity_percentage, nick],...]
        tl05 = []
        tl611 = []
        tl1217 = []
        tl1823 = []

        cscs05 = [0] * len(self._nicks)
        cscs611 = [0] * len(self._nicks)
        cscs1217 = [0] * len(self._nicks)
        cscs1823 = [0] * len(self._nicks)

        #count characters for each nick for each hours
        for line in self._slines:
            nick_index = self._nicks[line[1].lower()]
            # hours 0-5
            if line[0] >= 0 and line[0] <= 5:
                cscs05[nick_index] += len(line[2])
            # hours 6-11
            if line[0] >= 6 and line[0] <= 11:
                cscs611[nick_index] += len(line[2])
            # hours 12-17
            if line[0] >= 12 and line[0] <= 17:
                cscs1217[nick_index] += len(line[2])
            # hours 18-23
            if line[0] >= 18 and line[0] <= 23:
                cscs1823[nick_index] += len(line[2])

        #total characters for each hours
        cscs05sum = sum(cscs05)
        cscs611sum = sum(cscs611)
        cscs1217sum = sum(cscs1217)
        cscs1823sum = sum(cscs1823)

        #calculate activity percentages
        for i in self._nicks.items():
            #avoid division by zero
            if cscs05sum == 0:
                tl05.append([0.0, i[0]])
            else:
                tl05.append([cscs05[i[1]] / float(cscs05sum), i[0]])

            #avoid division by zero
            if cscs611sum == 0:
                tl611.append([0.0, i[0]])
            else:
                tl611.append([cscs611[i[1]] / float(cscs611sum), i[0]])

            #avoid division by zero
            if cscs1217sum == 0:
                tl1217.append([0.0, i[0]])
            else:
                tl1217.append([cscs1217[i[1]] / float(cscs1217sum), i[0]])

            #avoid division by zero
            if cscs1823sum == 0:
                tl1823.append([0.0, i[0]])
            else:
                tl1823.append([cscs1823[i[1]] / float(cscs1823sum), i[0]])

        #get sorted tuple lists and limit the list to limit nicks
        tls = []
        tls.append(sorted(tl05, key=itemgetter(0), reverse=True)[0:limit])
        tls.append(sorted(tl611, key=itemgetter(0), reverse=True)[0:limit])
        tls.append(sorted(tl1217, key=itemgetter(0), reverse=True)[0:limit])
        tls.append(sorted(tl1823, key=itemgetter(0), reverse=True)[0:limit])

        #calculate and add relative activities for each nick in the list
        for hour_list in tls:
            sumf = 0.0
            for i in hour_list:
                sumf += i[0]
            for i in hour_list:
                i.append(i[0] / sumf)

        return tls

    def get_most_used_words(self, limit):
        """Return a list of most used words.

        List consists limit amount of tuples (word, word_count)
        sorted by word count.

        Arguments:
        limit -- number of rows in the list

        """
        wcount = dict()

        for line in self._slines:
            for word in line[2].split():
                #match only letters
                if not word.isalpha():
                    continue

                #treat every word as lowercase
                word = word.lower()
                if word in wcount:
                    wcount[word] += 1
                else:
                    wcount[word] = 1

        return sorted(iter(wcount.items()), key=itemgetter(1), reverse=True)[0:limit]

    def get_most_ref_nicks(self, limit):
        """Return a list of most referenced nicks.

        List consists limit amount of tuples (nick, reference_count)
        sorted by reference count.

        Arguments:
        limit -- number of rows in the list

        """
        nfcount = dict()

        #look for references
        for nick in self._nicks.keys():
            for line in self._slines:
                #treat the line as lowercase
                lline = line[2].lower()
                # match only whole words
                if re.search(r'\b' + nick + r'\b', lline):
                    if nick in nfcount:
                        nfcount[nick] += 1
                    else:
                        nfcount[nick] = 1

        return sorted(iter(nfcount.items()), key=itemgetter(1), reverse=True)[0:limit]

class XHTMLGenerator:
    """Class for generating XHTML statistic pages from irc log files."""

    def __init__(self, log, chan_name, net_name):
        """Initialize log parser.

        Arguments:
        log -- Log instance of the irc log
        chan_name -- channel name
        net_name -- network name

        """

        #Log instance
        self._log = log

        #escape html entities in channel name and network name
        chan_name = escape(chan_name)
        net_name = escape(net_name)

        #xhtml main code
        self._main = '''<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-type" content="text/html; charset=UTF-8" />
<title>''' + chan_name + ' @ ' + net_name + ''' stats</title>
<style type="text/css">
body {background-color: #dedeee;font-family: sans-serif;font-size: 13px;color: black;}
div {text-align: center;}
table {margin-left: auto;margin-right: auto;max-width:80rem;}
td {background-color: #BABADD;padding: 2px 2px 2px 2px;text-align: left;vertical-align: bottom; }
th {background-color: #666699;color: white;border-color: black; border-style: solid solid solid solid;border-width: 1px 1px 1px 1px;padding: 2px 2px 2px 2px;}
</style>
</head>
<body>
<div>
''' + '<h1>IRC Stats for channel ' + chan_name + ' @ ' + net_name + '</h1>' + '''
<p>Statistics were generated on ''' + strftime("%Y-%m-%d %H:%M:%S") + '''.<br/>
During reporting period, total of <strong>''' + str(self._log.num_nicks) + '''</strong> different nicks have been spotted on ''' + chan_name + '''.</p>
'''
        #xhtml footer code
        self._footer = '''</div>
</body>
</html>
'''

    def write(self, filename):
        """Write irc stats XHTML page to a file.

        Arguments:
        filename -- filename of the XHTML page

        """
        f = open(filename, "w")
        try:
            f.write(self._main)
            f.write(self._footer)
        except IOError:
            print("ERROR: Unable to write XHTML file '%s'" % filename)
            exit()
        finally:
            f.close()

    def add_activity_by_hours(self):
        """Add XHTML code for a bar graph of channel activity."""
        hps = self._log.get_activity_by_hours()

        self._main += '''<h2>Channel activity by hours</h2>
<table>
<tr>
<th>0</th>
<th>1</th>
<th>2</th>
<th>3</th>
<th>4</th>
<th>5</th>
<th>6</th>
<th>7</th>
<th>8</th>
<th>9</th>
<th>10</th>
<th>11</th>
<th>12</th>
<th>13</th>
<th>14</th>
<th>15</th>
<th>16</th>
<th>17</th>
<th>18</th>
<th>19</th>
<th>20</th>
<th>21</th>
<th>22</th>
<th>23</th>
</tr>
<tr>
'''
        for hp in hps:
            self._main += '<td><img src="bar.png" width="38" height="' + str(int(hp * 1000)) + '''" alt=""/></td>
'''

        self._main += '''</tr>
<tr>
'''
        for hp in hps:
            self._main += '<td>' + "%.1f%%" % (hp * 100) + '''</td>
'''

        self._main += '''</tr>
</table>
'''

    def add_most_active_nicks(self, limit):
        """Add XHTML code for a list of most active nicks with additional statistics.

        Arguments:
        limit -- number of rows in the list

        """
        self._main += '''<h2>Most active nicks</h2>
<table>
<tr>
<th>#</th>
<th>Nick</th>
<th>Lines</th>
<th>Words</th>
<th>W/L</th>
<th>Characters</th>
<th>C/L</th>
<th>Latest quote</th>
</tr>
'''
        mans = self._log.get_most_active_nicks(limit)
        counter = 1
        for i in mans:
            self._main += '''<tr>
<td><strong>''' + str(counter) + '''</strong></td>
<td>''' + escape(i[0]) + '''</td>
<td>''' + str(i[1]) + '''</td>
<td>''' + str(i[2]) + '''</td>
<td>''' + "%.1f" % i[3] + '''</td>
<td>''' + str(i[4]) + '''</td>
<td>''' + "%.1f" % i[5] + '''</td>
<td>''' + escape(i[6]) + '''</td>
</tr>
'''
            counter += 1

        self._main += '''</table>
'''



    def add_most_active_nicks_by_hours(self, limit):
        """Add XHTML code for a list of most active nick by hours.

        Arguments:
        limit -- number of rows in the list

        """
        self._main += '''<h2>Most active nicks by hours</h2>
<table>
<tr>
<th>#</th>
<th>0-5</th>
<th>6-11</th>
<th>12-17</th>
<th>18-23</th>
</tr>
'''
        manhs = self._log.get_most_active_nicks_by_hours(limit)
        counter = 1
        for i in range(limit):
            self._main += '''<tr>
<td><strong>''' + str(counter) + '''</strong></td>
<td><img src="bar2.png" width="''' + str(int(manhs[0][i][2] * 300)) + '''" height="15" alt=""/> ''' + "%.1f%%" % (manhs[0][i][0] * 100) + ''' - ''' + escape(manhs[0][i][1]) + '''</td>
<td><img src="bar2.png" width="''' + str(int(manhs[1][i][2] * 300)) + '''" height="15" alt=""/> ''' + "%.1f%%" % (manhs[1][i][0] * 100) + ''' - ''' + escape(manhs[1][i][1]) + '''</td>
<td><img src="bar2.png" width="''' + str(int(manhs[2][i][2] * 300)) + '''" height="15" alt=""/> ''' + "%.1f%%" % (manhs[2][i][0] * 100) + ''' - ''' + escape(manhs[2][i][1]) + '''</td>
<td><img src="bar2.png" width="''' + str(int(manhs[3][i][2] * 300)) + '''" height="15" alt=""/> ''' + "%.1f%%" % (manhs[3][i][0] * 100) + ''' - ''' + escape(manhs[3][i][1]) + '''</td>
</tr>
'''
            counter += 1

        self._main += '''</table>
'''

    def add_most_used_words(self, limit):
        """Add XHTML code for list of most used words.

        Arguments:
        limit -- number of rows in the list

        """
        self._main += '''<h2>Most used words</h2>
<table>
<tr>
<th>#</th>
<th>Word</th>
<th>Number of uses</th>
</tr>
'''
        wcounts = self._log.get_most_used_words(limit)
        counter = 1
        for i in wcounts:
            self._main += '''<tr>
<td><strong>''' + str(counter) + '''</strong></td>
<td>''' + escape(i[0]) + '''</td>
<td>''' + str(i[1]) + '''</td>
</tr>
'''
            counter += 1

        self._main += '''</table>
'''

    def add_most_ref_nicks(self, limit):
        """
        Add XHTML code for list of most referenced nicks.

        Arguments:
        limit -- number of rows in the list

        """
        self._main += '''<h2>Most referenced nicks</h2>
<table>
<tr>
<th>#</th>
<th>Nick</th>
<th>Number of references</th>
</tr>
'''
        nfcounts = self._log.get_most_ref_nicks(limit)
        counter = 1
        for i in nfcounts:
            self._main += '''<tr>
<td><strong>''' + str(counter) + '''</strong></td>
<td>''' + escape(i[0]) + '''</td>
<td>''' + str(i[1]) + '''</td>
</tr>
'''
            counter += 1

        self._main += '''</table>
'''
