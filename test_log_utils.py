"""Test module for log_utils module"""

import unittest
import log_utils

class LogTest(unittest.TestCase):
    """Class for testing log_utils module"""

    def test_log(self):
        """Test _parser method"""
        log = log_utils.Log("test.log")

        self.assertEqual(log._slines, [(4, 'tokol', 'hrr prr prr'), \
                                       (4, 'jgnrr', 'hrr hrr'), \
                                       (4, 'jgnrr', 'Drr Drr'), \
                                       (4, 'jgnrr', 'mrr mrr'), \
                                       (6, 'ssg', 'derp derp')])

        self.assertEqual(log._nicks, {'tokol': 0, 'jgnrr': 1, 'ssg': 2})
        self.assertEqual(log.num_nicks, 3)
        self.assertEqual(log.get_most_used_words(2), [('hrr', 3), ('derp', 2)])

if __name__ == '__main__':
    unittest.main()
